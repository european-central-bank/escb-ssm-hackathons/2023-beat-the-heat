import os
import importlib
import pandas as pd
import utils.constants as cst

importlib.reload(cst)


def get_mask(dataframe, dimension, value):
    """
    Returns dataframe filtering mask
    """
    return dataframe[dimension] == value


def get_passthrough_mask(dataframe):
    """
    Returns dataframe filtering mask
    """
    return pd.Series(True, index=dataframe.index)


def get_data(
    namefile=None,
    geo=None,
    sector=None,
    year=None,
    variable=None,
    pivot=False,
    source_file_path="dataset_nuts3_2002-2018.csv",
):
    """
    Returns data according to selection criterias
    """

    # Get complete data
    complete_df = pd.read_csv(os.path.join(cst.FOLDER_ECONOMIC_DATA, source_file_path))

    # Create filtering mask
    mask = (
        (
            get_mask(complete_df, "namefile", namefile)
            if namefile is not None
            else get_passthrough_mask(complete_df)
        )
        & (
            get_mask(complete_df, "geo", geo)
            if geo is not None
            else get_passthrough_mask(complete_df)
        )
        & (
            get_mask(complete_df, "variable", variable)
            if variable is not None
            else get_passthrough_mask(complete_df)
        )
        & (
            get_mask(complete_df, "sector", sector)
            if sector is not None
            else get_passthrough_mask(complete_df)
        )
        & (
            get_mask(complete_df, "time", year)
            if year is not None
            else get_passthrough_mask(complete_df)
        )
    )

    df = complete_df.copy(deep=True)[mask]

    return df
