import os

ABSOLUTE_FOLDER_REPOSITORY = '/home/ec2-user/SageMaker/hackathon-repo-group07'
PERSONAL_FOLDER_RICCARDO = os.path.join(ABSOLUTE_FOLDER_REPOSITORY, 'riccardo')

ABSOLUTE_FOLDER_DATA = 's3://ecb-hackathon-data-group07-x19s00/'

FOLDER_CLIMATE_DATA = os.path.join(ABSOLUTE_FOLDER_DATA, 'climate_data')
FOLDER_ECONOMIC_DATA = os.path.join(ABSOLUTE_FOLDER_DATA, 'economic_data')
FOLDER_PROCESSED_DATA = os.path.join(ABSOLUTE_FOLDER_DATA, 'processed_data')
FOLDER_REGIONAL_BOUNDARIES = os.path.join(FOLDER_ECONOMIC_DATA, 'regional_boundaries')

FILE_REGIONAL_BOUNDARIES = 'NUTS_RG_20M_2021_3035.shp'