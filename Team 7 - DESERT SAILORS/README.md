- ***MainFolder***

Folder containing source code: utils, compiling pipelines, analytical notebooks


- ***Model***

Folder containing models, including notebooks and outputs


- ***peronal***

Folder containing participants personal developments (used for easier collaboration given GitCommit misfunctioning)


- ***data***

Folder containing locally compiled data, serving as input for analysis and modelling
