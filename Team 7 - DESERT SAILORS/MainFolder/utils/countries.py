import pkg_resources
import pandas as pd

EA = sorted(
    [
        "AT",
        "BE",
        "CY",
        "DE",
        "EE",
        "ES",
        "FI",
        "FR",
        "GR",
        "HR",
        "IE",
        "IT",
        "LT",
        "LU",
        "LV",
        "MT",
        "NL",
        "PT",
        "SI",
        "SK",
    ]
)

BU = sorted(EA + ["BG"])

EU = sorted(BU + ["CZ", "DK", "HU", "PL", "RO", "SE"])

EEA = sorted(EU + ["IS", "LI", "NO"])

EFTA = sorted(["IS", "LI", "NO", "CH"])

DB_FILE = pkg_resources.resource_filename("utils", "countries.dat")
df_country = pd.read_csv(DB_FILE, encoding="utf-8", dtype=str, na_filter=False)

ALPHA2_TO_COUNTRY = df_country.set_index("alpha2")["name"].to_dict()
ALPHA3_TO_COUNTRY = df_country.set_index("alpha3")["name"].to_dict()
ALPHA2_TO_ALPHA3 = df_country.set_index("alpha2")["alpha3"].to_dict()
ALPHA3_TO_ALPHA2 = df_country.set_index("alpha3")["alpha2"].to_dict()

COUNTRY_TO_ALPHA2 = {v: k for k, v in ALPHA2_TO_COUNTRY.items()}

EA_START_END = {
    "AT": {"start": 1999, "end": 9999},
    "BE": {"start": 1999, "end": 9999},
    "CY": {"start": 2008, "end": 9999},
    "DE": {"start": 1999, "end": 9999},
    "EE": {"start": 2011, "end": 9999},
    "ES": {"start": 1999, "end": 9999},
    "FI": {"start": 1999, "end": 9999},
    "FR": {"start": 1999, "end": 9999},
    "GR": {"start": 2001, "end": 9999},
    "HR": {"start": 2023, "end": 9999},
    "IE": {"start": 1999, "end": 9999},
    "IT": {"start": 1999, "end": 9999},
    "LT": {"start": 2015, "end": 9999},
    "LU": {"start": 1999, "end": 9999},
    "LV": {"start": 2014, "end": 9999},
    "MT": {"start": 2008, "end": 9999},
    "NL": {"start": 1999, "end": 9999},
    "PT": {"start": 1999, "end": 9999},
    "SI": {"start": 2007, "end": 9999},
    "SK": {"start": 2009, "end": 9999},
}


EUROPE = df_country[df_country.continent == "Europe"].alpha2.to_list()
NORTH_AMERICA = df_country[df_country.continent == "North America"].alpha2.to_list()
SOUTH_AMERICA = df_country[df_country.continent == "South America"].alpha2.to_list()
AMERICAS = NORTH_AMERICA + SOUTH_AMERICA

ASIA = df_country[df_country.continent == "Asia"].alpha2.to_list()
AFRICA = df_country[df_country.continent == "Africa"].alpha2.to_list()
OCEANIA = df_country[df_country.continent == "Oceania"].alpha2.to_list()
ANTARTICA = df_country[df_country.continent == "Antarctica"].alpha2.to_list()

ALPHA2_TO_CONTINENT = {
    **{c: "Asia" for c in ASIA},
    **{c: "Europe" for c in EUROPE},
    **{c: "North America" for c in NORTH_AMERICA},
    **{c: "South America" for c in SOUTH_AMERICA},
    **{c: "Africa" for c in AFRICA},
    **{c: "Oceania" for c in OCEANIA},
    **{c: "Antarctica" for c in ANTARTICA},
}
