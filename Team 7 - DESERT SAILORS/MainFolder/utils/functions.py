import geopandas as gpd
import matplotlib.pyplot as plt
import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature

from utils.constants import *
from utils.countries import EU


def load_nuts_mask(level=3, subset=EU):
    if subset is not None:
        if "GR" in subset:
            subset[subset.index("GR")] = "EL"

    nuts = gpd.read_file(
        os.path.join(FOLDER_REGIONAL_BOUNDARIES, FILE_REGIONAL_BOUNDARIES)
    )

    nuts = nuts[nuts["LEVL_CODE"] == level]

    if subset is not None:
        nuts = nuts[nuts["CNTR_CODE"].isin(subset)]

    nuts = nuts[["CNTR_CODE", "NUTS_ID", "NUTS_NAME", "geometry"]]
    nuts = nuts.sort_values("NUTS_ID")

    nuts.to_crs(4326, inplace=True)
    if subset is not None:
        nuts = nuts.cx[-15:45, 30:75]
    else:
        nuts = nuts.cx[-25:45, 30:90]
    nuts.to_crs(3035, inplace=True)

    return nuts.sort_values(["CNTR_CODE", "NUTS_ID"])


def plot_map(df, variable, cmap, title, output_file_name, save=False):
    df = df.to_crs(4326)

    ax = plt.axes(projection=ccrs.PlateCarree())

    ax.add_feature(cfeature.BORDERS, alpha=0.4)
    ax.add_feature(cfeature.LAND)
    ax.add_feature(cfeature.OCEAN)
    ax.add_feature(cfeature.COASTLINE, alpha=0.4)

    gl = ax.gridlines(
        crs=ccrs.PlateCarree(),
        draw_labels=True,
        linewidth=0.5,
        color="black",
        alpha=0.2,
        linestyle="--",
    )
    gl.top_labels = False
    gl.left_labels = True
    gl.right_labels = False
    gl.xformatter = cartopy.mpl.gridliner.LONGITUDE_FORMATTER
    gl.yformatter = cartopy.mpl.gridliner.LATITUDE_FORMATTER

    df.plot(ax=ax, column=variable, cmap=cmap, legend=True)

    plt.title(title)
    plt.gcf().set_size_inches(6, 6)

    if save:
        plt.savefig(
            os.path.join("MainFolder", "charts", output_file_name + ".svg"),
            dpi=1200,
            bbox_inches="tight",
        )
        plt.close()
    else:
        plt.show()
