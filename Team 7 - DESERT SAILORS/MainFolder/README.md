# Description of the codes and how to run the pipeline

**Refer to the `Model` folder for codes related to model training**

1) process_climate_data: ingestion of the climate data and union into a unique dataset
2) process_climate_data_yearly: aggregation at yearly frequency and computation of relevant variables. Creation of the darought indicator
3) summary_climate_data: summary statistics used to overview and exploring data patterns

4) bundle-economic-data: economic data ingestion
5) compile-economic-data: aggregation of economic data?
6) combine_all: creation of training and testing dataset by combining climate and economic data already aggregated at yearly level

7) drought_methodology: case study on droughts and mehtodology for agriculture and manufacturing droughts
8) heatwaves-indicators: computation of yearly indicators for heat waves from daily data
9) plot_HW: case study for heat waves around Europe