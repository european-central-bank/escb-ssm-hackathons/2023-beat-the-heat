# Introduction
This repository documents the outcome of the hackathon [Beat the Heat](https://innovation.banque-france.fr/fr/challenges/climate-change-challenge-banque-france-ecb?lang=en), the ESCB/SSM hackathon on climate change. 

# Background

In November 2023, the European Central Bank, Banque de France, Banca d'Italia, and the European Commission (Copernicus Programme) held a hackathon on the impact of extreme weather (droughts and extreme heat) on the economy (agricultural and industrial production). The event was hosted on ECB premises in Frankfurt. 
62 players were selected from central banks and authorities within the European System of Central Banks and the Single Supervisory Mechanism. Eight teams were composed with diverse skills in coding and analytics, economics and climate change data.
Within 48 hours, the hackathon aimed to address the following question: What is the impact of droughts and heat waves or their combined impact on industrial value added in the EU? 

# Deliverables of the hackathon
The teams were asked to address the following:

In a first step in explanatory data analysis, using ML methods, revealing patterns and importance of different features and characteristics of the data (i.e. answer questions on which factors are important and why); in particular, uncovering spatially dependent patterns of industrial production dependency on drought and heat stress.
In a second step, creating a model with the best predictive power; in particular, in predicting the impact of drought and heat wave on industrial production. The models developed should be spatially dependent, as industrial production varies across different countries/regions and responds differently to climate shocks.

The hackathon participants were encouraged to derive innovative ML models to characterize the impact of either simple events (drought or heat stress separately) or their common impacts (as a compound event).

# Data
The participants were provided with dataset from the following sources:
* Economic data from Eurostat [National Accounts](https://ec.europa.eu/eurostat/web/national-accounts) in the "General and regional statistics".

* Climate data and indicators from the European Drought Observatory, prepared by the European Commission (Copernicus Programme), the [ECMWF Reanalysis v5](https://www.ecmwf.int/en/forecasts/dataset/ecmwf-reanalysis-v5) (ERA5) data. The data was processed to fit the needs of the Hackathon.

# Environment
The developments were completed using Amazon Web Services.

# Code

Codes were developed as Python, R or Jupyter notebooks. 
The code delivered by each team is provided via the following links :
* [Team 1 - CLIMATE CHANGE CHALLENGERS](Team 1 - CLIMATE CHANGE CHALLENGERS)
* [Team 2 - EXPLODING HACKERS](Team 2 - EXPLODING HACKERS)
* [Team 3 - THE HOT MODELLERS GROWING TREES](Team 3 - THE HOT MODELLERS GROWING TREES)
* [Team 4 - HOT FOR COFFEE](Team 4 - HOT FOR COFFEE)
* [Team 5 - GREEN DREAM](Team 5 - GREEN DREAM)
* [Team 6 - 6°C](Team 6 - 6°C)
* [Team 7 - DESERT SAILORS](Team 7 - DESERT SAILORS)
* [Team 8 - CLIMATE CHALLENGERS](Team 8 - CLIMATE CHALLENGERS)
The code is not maintained. 

# License

The code is licensed under the European Union Public License 1.2 ([EUPL-1.2](https://spdx.org/licenses/EUPL-1.2) or in [this repository](EUPL-1.2-EN.txt)).

# Contact
General questions about the "Beat the Heat" Hackathon and the use of data can be submitted to [C3@ecb.europa.eu](mailto:C3@ecb.europa.eu).

