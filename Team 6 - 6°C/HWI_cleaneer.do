global rawdir "/home/gabriele/Downloads/"
global cleandir "/home/gabriele/Downloads/"

// Heat wave intensity data
insheet using "${rawdir}/intensity_2001-2022.csv", names clear
duplicates drop 
g day = date(substr(date, 1, 10), "YMD")
format day %td
egen nuts_num=group(nuts_id)
preserve // crazy stuff: Stata does not accept a string xtset, so I keep the wankthrough and merge it back afterwards
	tempfile StataSucks
	keep nuts_num nuts_id
	duplicates drop
	save `StataSucks'
restore
xtset nuts_num day
tsfill, full
g mofd = mofd(day)
g qofd = qofd(day)
g month = month(day)
g quarter = quarter(day)
replace year = year(day)
ren median Intensity
replace Intensity = 0 if mi(Intensity)
g HWI = (Intensity > 0)
merge m:1 nuts_num using `StataSucks', update
preserve // monthly data
	bys nuts_id mofd (day): g spell_ind = (HWI == 1 & HWI[_n-1] == 0)
	g spell_duration = 3 if spell_ind == 1 
	bys nuts_id mofd (day): replace spell_duration = spell_duration[_n-1] + 1 if HWI == 1 & spell_ind == 0
	collapse (sum) HWI Intensity spell_ind (max) spell_duration, by(nuts_id year month) fast
	replace HWI = HWI + 2 if spell_ind > 0
	replace spell_duration = 0 if mi(spell_duration)
	ren (HWI Intensity spell_ind spell_duration) (=_m)
	reshape wide HWI Intensity spell_ind spell_duration, i(nuts_id year) j(month)
	save "${rawdir}/HWI_wide_monthly", replace
restore, preserve // quarterly data
	bys nuts_id qofd (day): g spell_ind = (HWI == 1 & HWI[_n-1] == 0)
	g spell_duration = 3 if spell_ind == 1 
	bys nuts_id qofd (day): replace spell_duration = spell_duration[_n-1] + 1 if HWI == 1 & spell_ind == 0
	collapse (sum) HWI Intensity spell_ind (max) spell_duration, by(nuts_id year quarter) fast
	replace HWI = HWI + 2 if spell_ind > 0
	replace spell_duration = 0 if mi(spell_duration)
	ren (HWI Intensity spell_ind spell_duration) (=_q)
	reshape wide HWI Intensity spell_ind spell_duration, i(nuts_id year) j(quarter)
	save "${rawdir}/HWI_wide_quarterly", replace
restore

bys nuts_id (day): g spell_ind = (HWI == 1 & HWI[_n-1] == 0)
g spell_duration = 3 if spell_ind == 1 
bys nuts_id (day): replace spell_duration = spell_duration[_n-1] + 1 if HWI == 1 & spell_ind == 0
collapse (sum) HWI Intensity spell_ind (max) spell_duration, by(nuts_id year) fast
replace HWI = HWI + 2 if spell_ind > 0
replace spell_duration = 0 if mi(spell_duration)

merge 1:1 nuts_id year using "${rawdir}/HWI_wide_monthly", nogen keep(3)
merge 1:1 nuts_id year using "${rawdir}/HWI_wide_quarterly", nogen keep(3)

ren HWI HWI_days
egen nuts_num=group(nuts_id)
xtset nuts_num year
g HWI_days_l = l.HWI_days
drop nuts_num
outsheet using "${cleandir}/HWI_wide.csv", replace delimiter(",")


// SOIL MOISTURE ANOMALY
forv y = 2001/2022{
	insheet using "${rawdir}/smian_NUTS3_MEDIAN_`y'.csv", names clear
	save "${rawdir}/smian_`y'", replace
}
clear
forv y = 2001/2022{
	append using "${rawdir}/smian_`y'", force
}

g date = date(timestamp, "YMD")
g year = year(date)
g mofd = mofd(date)
g month = month(date)
g qofd = qofd(date)
g quarter = quarter(date)
g drought = (median < -1)
ren median SMA
ren nuts_code nuts_id
preserve // monthly
	collapse (mean) SMA (min) minSMA=SMA (max) maxSMA=SMA (sum) drought, by(nuts_id year month) fast
	ren (SMA minSMA maxSMA drought) (=_m)
	reshape wide SMA minSMA maxSMA drought, i(nuts_id year) j(month)
	save "${rawdir}/SMA_wide_monthly", replace
restore, preserve // quarterly
	collapse (mean) SMA (min) minSMA=SMA (max) maxSMA=SMA (sum) drought, by(nuts_id year quarter) fast
	ren (SMA minSMA maxSMA drought) (=_q)
	reshape wide SMA minSMA maxSMA drought, i(nuts_id year) j(quarter)
	save "${rawdir}/SMA_wide_quarterly", replace
restore

collapse (mean) SMA (min) minSMA=SMA (max) maxSMA=SMA (sum) drought, by(nuts_id year) fast

merge 1:1 nuts_id year using "${rawdir}/SMA_wide_monthly", nogen keep(3)
merge 1:1 nuts_id year using "${rawdir}/SMA_wide_quarterly", nogen keep(3)

egen nuts_num=group(nuts_id)
xtset nuts_num year
g SMA_lag = l.SMA
drop nuts_num
outsheet using "${cleandir}/SMA_wide.csv", replace delimiter(",")

// TEMPERATURE MAX
forv y = 2001/2022{
	insheet using "${rawdir}/climate_data/temp_max/tpmax_NUTS3_MEDIAN_`y'.csv", names clear
	save "${rawdir}/tpmax_`y'", replace
}
clear
forv y = 2001/2022{
	append using "${rawdir}/tpmax_`y'", force
}

g date = date(timestamp, "YMD")
g year = year(date)
g mofd = mofd(date)
g month = month(date)
g qofd = qofd(date)
g quarter = quarter(date)
ren median tpmax
ren nuts_code nuts_id
preserve // monthly
	collapse (mean) tpmax (min) mintpmax=tpmax (max) maxtpmax=tpmax, by(nuts_id year month) fast
	ren (tpmax mintpmax maxtpmax) (=_m)
	reshape wide tpmax mintpmax maxtpmax, i(nuts_id year) j(month)
	save "${rawdir}/TPMAX_wide_monthly", replace
restore, preserve // quarterly
	collapse (mean) tpmax (min) mintpmax=tpmax (max) maxtpmax=tpmax, by(nuts_id year quarter) fast
	ren (tpmax mintpmax maxtpmax) (=_q)
	reshape wide tpmax mintpmax maxtpmax, i(nuts_id year) j(quarter)
	save "${rawdir}/TPMAX_wide_quarterly", replace
restore

collapse (mean) tpmax (min) mintpmax=tpmax (max) maxtpmax=tpmax, by(nuts_id year) fast

merge 1:1 nuts_id year using "${rawdir}/TPMAX_wide_monthly", nogen keep(3)
merge 1:1 nuts_id year using "${rawdir}/TPMAX_wide_quarterly", nogen keep(3)
egen nuts_num=group(nuts_id)
xtset nuts_num year
g tpmax_lag = l.tpmax
drop nuts_num
outsheet using "${cleandir}/TPMAX_wide.csv", replace delimiter(",")

// LOW FLOW INDEX
forv y = 2001/2022{
	insheet using "${rawdir}/climate_data/low_flow_index/lfinx_NUTS3_MAX_`y'.csv", names clear
	save "${rawdir}/lnfix_`y'", replace
}
clear
forv y = 2001/2022{
	append using "${rawdir}/lnfix_`y'", force
}

g date = date(timestamp, "YMD")
g year = year(date)
g mofd = mofd(date)
g month = month(date)
g qofd = qofd(date)
g quarter = quarter(date)
ren monthly_max lfinx
g critical_value_lfinx = lfinx >= 0.5
*ren nuts_code nuts_id
preserve // monthly
	collapse (mean) lfinx, by(nuts_id year month) fast
	ren (lfinx) (=_m)
	reshape wide lfinx, i(nuts_id year) j(month)
	save "${rawdir}/LFINX_wide_monthly", replace
restore, preserve // quarterly
	collapse (mean) lfinx (min) minlfinx=lfinx (max) maxlfinx=lfinx (sum) critical_value_lfinx, by(nuts_id year quarter) fast
	ren (lfinx minlfinx maxlfinx  critical_value_lfinx) (=_q)
	reshape wide lfinx minlfinx maxlfinx  critical_value_lfinx, i(nuts_id year) j(quarter)
	save "${rawdir}/LFINX_wide_quarterly", replace
restore
collapse (mean) lfinx (min) minlfinx=lfinx (max) maxlfinx=lfinx (sum)  critical_value_lfinx, by(nuts_id year) fast
merge 1:1 nuts_id year using "${rawdir}/LFINX_wide_monthly", nogen keep(3)
merge 1:1 nuts_id year using "${rawdir}/LFINX_wide_quarterly", nogen keep(3)
egen nuts_num=group(nuts_id)
xtset nuts_num year
g lfinx_lag = l.lfinx
drop nuts_num
outsheet using "${cleandir}/LFINX_wide.csv", replace delimiter(",")


// FAPAR
forv y = 2012/2022{
	insheet using "${rawdir}/climate_data/absorbed_photosynthetically_active_radiation_anomaly/fpanv_NUTS3_MEDIAN_`y'.csv", names clear
	save "${rawdir}/fpanv_`y'", replace
}
clear
forv y = 2012/2022{
	append using "${rawdir}/fpanv_`y'", force
}
g date = date(timestamp, "YMD")
g year = year(date)
g mofd = mofd(date)
g month = month(date)
g qofd = qofd(date)
g quarter = quarter(date)
ren median fpanv
g critical_value_fpanv = (fpanv < -1)
*ren nuts_code nuts_id
preserve // monthly
	collapse (mean) fpanv , by(nuts_id year month) fast
	ren (fpanv) (=_m)
	reshape wide fpanv, i(nuts_id year) j(month)
	save "${rawdir}/FPANV_wide_monthly", replace
restore, preserve // quarterly
	collapse (mean) fpanv (min) minfpanv=fpanv (max) maxfpanv=fpanv (sum) critical_value_fpanv, by(nuts_id year quarter) fast
	ren (fpanv minfpanv maxfpanv critical_value_fpanv) (=_q)
	reshape wide fpanv minfpanv maxfpanv critical_value_fpanv, i(nuts_id year) j(quarter)
	save "${rawdir}/FPANV_wide_quarterly", replace
restore
collapse (mean) fpanv (min) minfpanv=fpanv (max) maxfpanv=fpanv (sum) critical_value_fpanv, by(nuts_id year) fast
merge 1:1 nuts_id year using "${rawdir}/FPANV_wide_monthly", nogen keep(3)
merge 1:1 nuts_id year using "${rawdir}/FPANV_wide_quarterly", nogen keep(3)
egen nuts_num=group(nuts_id)
xtset nuts_num year
g fpanv_lag = l.fpanv
drop nuts_num
outsheet using "${cleandir}/FPANV_wide.csv", replace delimiter(",")

// SPI DATA 
forv y = 2001/2022{
	insheet using "${rawdir}/climate_data/spi/spa01_NUTS3_MEDIAN_`y'.csv", names clear
	save "${rawdir}/spi01_`y'", replace
}
clear
forv y = 2001/2022{
	append using "${rawdir}/spi01_`y'", force
}

g date = date(timestamp, "YMD")
g year = year(date)
g mofd = mofd(date)
g month = month(date)
g qofd = qofd(date)
g quarter = quarter(date)
ren median spi01
g critical_value_spi01 = (spi01 < -1.5)
*ren nuts_code nuts_id
preserve // monthly
	collapse (mean) spi01 critical_value, by(nuts_id year month) fast
	ren (spi01 critical_value_spi01) (=_m)
	reshape wide spi01 critical_value, i(nuts_id year) j(month)
	save "${rawdir}/SPI01_wide_monthly", replace
restore, preserve // quarterly
	collapse (mean) spi01 (min) minspi01=spi01 (max) maxspi01=spi01 (sum) critical_value_spi01, by(nuts_id year quarter) fast
	ren (spi01 minspi01 maxspi01  critical_value_spi01) (=_q)
	reshape wide spi01 minspi01 maxspi01  critical_value_spi01, i(nuts_id year) j(quarter)
	save "${rawdir}/SPI01_wide_quarterly", replace
restore

collapse (mean) spi01 (min) minspi01=spi01 (max) maxspi01=spi01 (sum) critical_value_spi01, by(nuts_id year) fast

merge 1:1 nuts_id year using "${rawdir}/SPI01_wide_monthly", nogen keep(3)
merge 1:1 nuts_id year using "${rawdir}/SPI01_wide_quarterly", nogen keep(3)
egen nuts_num=group(nuts_id)
xtset nuts_num year
g spi01_lag = l.spi01
g minspi01_lag = l.minspi01
g maxspi01_lag = l.maxspi01
drop nuts_num
outsheet using "${cleandir}/SPI01_wide.csv", replace delimiter(",")

// SPI03
forv y = 2001/2022{
	insheet using "/home/gabriele/Downloads/climate_data/spi/spa03_NUTS3_MEDIAN_`y'.csv", names clear
	save "/home/gabriele/Downloads/spi03_`y'", replace
}
clear
forv y = 2001/2022{
	append using "/home/gabriele/Downloads/spi03_`y'", force
}

g date = date(timestamp, "YMD")
g year = year(date)
g mofd = mofd(date)
g month = month(date)
g qofd = qofd(date)
g quarter = quarter(date)
ren median spi03
g critical_value_spi03 = (spi03 < -1.5)
*ren nuts_code nuts_id
preserve // monthly
	collapse (mean) spi03 critical_value, by(nuts_id year month) fast
	ren (spi03 critical_value_spi03) (=_m)
	reshape wide spi03 critical_value, i(nuts_id year) j(month)
	save "/home/gabriele/Downloads/SPI03_wide_monthly", replace
restore, preserve // quarterly
	collapse (mean) spi03 (min) minspi03=spi03 (max) maxspi03=spi03 (sum) critical_value_spi03, by(nuts_id year quarter) fast
	ren (spi03 minspi03 maxspi03  critical_value_spi03) (=_q)
	reshape wide spi03 minspi03 maxspi03  critical_value_spi03, i(nuts_id year) j(quarter)
	save "/home/gabriele/Downloads/SPI03_wide_quarterly", replace
restore

collapse (mean) spi03 (min) minspi03=spi03 (max) maxspi03=spi03 (sum) critical_value_spi03, by(nuts_id year) fast

merge 1:1 nuts_id year using "/home/gabriele/Downloads/SPI03_wide_monthly", nogen keep(3)
merge 1:1 nuts_id year using "/home/gabriele/Downloads/SPI03_wide_quarterly", nogen keep(3)
egen nuts_num=group(nuts_id)
xtset nuts_num year
g spi03_lag = l.spi03
g minspi03_lag = l.minspi03
g maxspi03_lag = l.maxspi03
drop nuts_num
outsheet using "/home/gabriele/Downloads/SPI03_wide.csv", replace delimiter(",")

// SPI 06
forv y = 2001/2022{
	insheet using "/home/gabriele/Downloads/climate_data/spi/spa06_NUTS3_MEDIAN_`y'.csv", names clear
	save "/home/gabriele/Downloads/spi06_`y'", replace
}
clear
forv y = 2001/2022{
	append using "/home/gabriele/Downloads/spi06_`y'", force
}

g date = date(timestamp, "YMD")
g year = year(date)
g mofd = mofd(date)
g month = month(date)
g qofd = qofd(date)
g quarter = quarter(date)
ren median spi06
g critical_value_spi06 = (spi06 < -1.5)
*ren nuts_code nuts_id
preserve // monthly
	collapse (mean) spi06 critical_value, by(nuts_id year month) fast
	ren (spi06 critical_value_spi06) (=_m)
	reshape wide spi06 critical_value, i(nuts_id year) j(month)
	save "/home/gabriele/Downloads/SPI06_wide_monthly", replace
restore, preserve // quarterly
	collapse (mean) spi06 (min) minspi06=spi06 (max) maxspi06=spi06 (sum) critical_value_spi06, by(nuts_id year quarter) fast
	ren (spi06 minspi06 maxspi06  critical_value_spi06) (=_q)
	reshape wide spi06 minspi06 maxspi06  critical_value_spi06, i(nuts_id year) j(quarter)
	save "/home/gabriele/Downloads/SPI06_wide_quarterly", replace
restore

collapse (mean) spi06 (min) minspi06=spi06 (max) maxspi06=spi06 (sum) critical_value_spi06, by(nuts_id year) fast

merge 1:1 nuts_id year using "/home/gabriele/Downloads/SPI06_wide_monthly", nogen keep(3)
merge 1:1 nuts_id year using "/home/gabriele/Downloads/SPI06_wide_quarterly", nogen keep(3)
egen nuts_num=group(nuts_id)
xtset nuts_num year
g spi06_lag = l.spi06
g minspi06_lag = l.minspi06
g maxspi06_lag = l.maxspi06
drop nuts_num
outsheet using "/home/gabriele/Downloads/SPI06_wide.csv", replace delimiter(",")

// SPI 12
forv y = 2001/2021{
	insheet using "/home/gabriele/Downloads/climate_data/spi/spa12_NUTS3_MEDIAN_`y'.csv", names clear
	save "/home/gabriele/Downloads/spi12_`y'", replace
}
clear
forv y = 2001/2021{
	append using "/home/gabriele/Downloads/spi12_`y'", force
}

g date = date(timestamp, "YMD")
g year = year(date)
g mofd = mofd(date)
g month = month(date)
g qofd = qofd(date)
g quarter = quarter(date)
ren median spi12
g critical_value_spi12 = (spi12 < -1.5)
*ren nuts_code nuts_id
preserve // monthly
	collapse (mean) spi12 critical_value, by(nuts_id year month) fast
	ren (spi12 critical_value_spi12) (=_m)
	reshape wide spi12 critical_value, i(nuts_id year) j(month)
	save "/home/gabriele/Downloads/SPI12_wide_monthly", replace
restore, preserve // quarterly
	collapse (mean) spi12 (min) minspi12=spi12 (max) maxspi12=spi12 (sum) critical_value_spi12, by(nuts_id year quarter) fast
	ren (spi12 minspi12 maxspi12  critical_value_spi12) (=_q)
	reshape wide spi12 minspi12 maxspi12  critical_value_spi12, i(nuts_id year) j(quarter)
	save "/home/gabriele/Downloads/SPI12_wide_quarterly", replace
restore

collapse (mean) spi12 (min) minspi12=spi12 (max) maxspi12=spi12 (sum) critical_value_spi12, by(nuts_id year) fast

merge 1:1 nuts_id year using "/home/gabriele/Downloads/SPI12_wide_monthly", nogen keep(3)
merge 1:1 nuts_id year using "/home/gabriele/Downloads/SPI12_wide_quarterly", nogen keep(3)
egen nuts_num=group(nuts_id)
xtset nuts_num year
g spi12_lag = l.spi12
g minspi12_lag = l.minspi12
g maxspi12_lag = l.maxspi12
drop nuts_num
outsheet using "/home/gabriele/Downloads/SPI12_wide.csv", replace delimiter(",")


