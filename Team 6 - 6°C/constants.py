# This file contains all the hardcoded paths that are used in the project

import os
#base_path: str = 's3://ecb-hackathon-data-group06-x19s00'

class Paths:

    def __init__(self,
                 base_path: str = '/home/ec2-user/SageMaker/hackathon-repo-group06/Project/data',
                 data_path: str = '/home/ec2-user/SageMaker/hackathon-repo-group06/Project/data'):

        # Base path:

        self.Source = base_path

        # Base subfolders:

        self.EconomicData = os.path.join(self.Source, 'economic_data')
        self.ClimateData = os.path.join(self.Source, 'climate_data')
        
        # Input files inside Economic Data:

        self.AREA = os.path.join(self.EconomicData, 'area.xlsx')
        self.EMPLOYMENT = os.path.join(self.EconomicData, 'employment.xlsx')
        self.GDP = os.path.join(self.EconomicData, 'gdp.xlsx')
        self.GVA = os.path.join(self.EconomicData, 'gross_value_added.xlsx')
        self.POPULATION = os.path.join(self.EconomicData, 'population.xlsx')
        self.DATASET = os.path.join(self.EconomicData, 'dataset_nuts3_2002-2018.csv')
        self.SHAPE = os.path.join(self.EconomicData, 'regional_boundaries', 'NUTS_RG_20M_2021_3035.shp')
        
        self.DATASET_FULL1 = os.path.join(self.EconomicData, 'dataset_nuts3_2002-2018.csv')
        self.DATASET_FULL2 = os.path.join(self.EconomicData, 'dataset_nuts3_2019-2021.csv')

        # Subfolders inside Climate Data:

        self.FAPAR = os.path.join(self.ClimateData, 'absorbed_photosynthetically_active_radiation_anomaly')
        self.HeatIntensity = os.path.join(self.ClimateData, 'heat_intensity')
        self.LFI = os.path.join(self.ClimateData, 'low_flow_index')
        self.SMA = os.path.join(self.ClimateData, 'soil_mosture_anomaly')
        self.SPI = os.path.join(self.ClimateData, 'spi')
        self.TempMax = os.path.join(self.ClimateData, 'temp_max')

        # Input files inside Climate Data subfolders:

        self.FAPAR_2012 = os.path.join(self.FAPAR, 'fpanv_NUTS3_MEDIAN_2012.csv')
        self.FAPAR_2013 = os.path.join(self.FAPAR, 'fpanv_NUTS3_MEDIAN_2013.csv')
        self.FAPAR_2014 = os.path.join(self.FAPAR, 'fpanv_NUTS3_MEDIAN_2014.csv')
        self.FAPAR_2015 = os.path.join(self.FAPAR, 'fpanv_NUTS3_MEDIAN_2015.csv')
        self.FAPAR_2016 = os.path.join(self.FAPAR, 'fpanv_NUTS3_MEDIAN_2016.csv')
        self.FAPAR_2017 = os.path.join(self.FAPAR, 'fpanv_NUTS3_MEDIAN_2017.csv')
        self.FAPAR_2018 = os.path.join(self.FAPAR, 'fpanv_NUTS3_MEDIAN_2018.csv')

        self.HWI = os.path.join(self.HeatIntensity, 'íntensity_2001_2022.csv')

        self.LFI_2001 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2001.csv')
        self.LFI_2002 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2002.csv')
        self.LFI_2003 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2003.csv')
        self.LFI_2004 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2004.csv')
        self.LFI_2005 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2005.csv')
        self.LFI_2006 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2006.csv')
        self.LFI_2007 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2007.csv')
        self.LFI_2008 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2008.csv')
        self.LFI_2009 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2009.csv')
        self.LFI_2010 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2010.csv')
        self.LFI_2011 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2011.csv')
        self.LFI_2012 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2012.csv')
        self.LFI_2013 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2013.csv')
        self.LFI_2014 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2014.csv')
        self.LFI_2015 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2015.csv')
        self.LFI_2016 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2016.csv')
        self.LFI_2017 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2017.csv')
        self.LFI_2018 = os.path.join(self.LFI, 'lfinx_NUTS3_MAX_2018.csv')

        self.SMA_2001 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2001.csv')
        self.SMA_2002 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2002.csv')
        self.SMA_2003 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2003.csv')
        self.SMA_2004 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2004.csv')
        self.SMA_2005 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2005.csv')
        self.SMA_2006 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2006.csv')
        self.SMA_2007 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2007.csv')
        self.SMA_2008 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2008.csv')
        self.SMA_2009 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2009.csv')
        self.SMA_2010 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2010.csv')
        self.SMA_2011 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2011.csv')
        self.SMA_2012 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2012.csv')
        self.SMA_2013 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2013.csv')
        self.SMA_2014 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2014.csv')
        self.SMA_2015 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2015.csv')
        self.SMA_2016 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2016.csv')
        self.SMA_2017 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2017.csv')
        self.SMA_2018 = os.path.join(self.SMA, 'smian_NUTS3_MEDIAN_2018.csv')

        self.SPI_2001 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2001.csv')
        self.SPI_2002 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2002.csv')
        self.SPI_2003 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2003.csv')
        self.SPI_2004 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2004.csv')
        self.SPI_2005 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2005.csv')
        self.SPI_2006 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2006.csv')
        self.SPI_2007 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2007.csv')
        self.SPI_2008 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2008.csv')
        self.SPI_2009 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2009.csv')
        self.SPI_2010 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2010.csv')
        self.SPI_2011 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2011.csv')
        self.SPI_2012 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2012.csv')
        self.SPI_2013 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2013.csv')
        self.SPI_2014 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2014.csv')
        self.SPI_2015 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2015.csv')
        self.SPI_2016 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2016.csv')
        self.SPI_2017 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2017.csv')
        self.SPI_2018 = os.path.join(self.SPI, 'spa01_NUTS3_MEDIAN_2018.csv')

        self.TM_2001 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2001.csv')
        self.TM_2002 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2002.csv')
        self.TM_2003 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2003.csv')
        self.TM_2004 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2004.csv')
        self.TM_2005 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2005.csv')
        self.TM_2006 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2006.csv')
        self.TM_2007 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2007.csv')
        self.TM_2008 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2008.csv')
        self.TM_2009 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2009.csv')
        self.TM_2010 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2010.csv')
        self.TM_2011 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2011.csv')
        self.TM_2012 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2012.csv')
        self.TM_2013 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2013.csv')
        self.TM_2014 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2014.csv')
        self.TM_2015 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2015.csv')
        self.TM_2016 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2016.csv')
        self.TM_2017 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2017.csv')
        self.TM_2018 = os.path.join(self.TempMax, 'tpmax_NUTS3_MEDIAN_2018.csv')

        # Data path:

        self.Data = data_path

        # Intermediate and output files inside Economic Data:

        self.CleanEconomicData = os.path.join(self.Data, 'processed_economic_data.csv')
        self.CleanClimateData = os.path.join(self.Data, 'processed_climate_data.csv')
        self.CleanMergedData = os.path.join(self.Data, 'clean_data.parquet')
        self.CleanMergedDataCSV = os.path.join(self.Data, 'clean_data.csv')

        self.OutputData = os.path.join(self.Data, 'Test_06.csv')